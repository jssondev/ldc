var slider1 = new IdealImageSlider.Slider({
    selector: '#slider1',
    height: '7:2', // "auto" | px value (e.g. 400) | aspect ratio (e.g. "16:9")
    initialHeight: 400, // for "auto" and aspect ratio
    maxHeight: null, // for "auto" and aspect ratio
    interval: 4000,
    transitionDuration: 700,
    effect: 'slide',
    disableNav: false,
    keyboardNav: true,
    previousNavSelector: '',
    nextNavSelector: '',
    classes: {
      container: 'ideal-image-slider',
      slide: 'iis-slide',
      previousSlide: 'iis-previous-slide',
      currentSlide: 'iis-current-slide',
      nextSlide: 'iis-next-slide',
      previousNav: 'iis-previous-nav',
      nextNav: 'iis-next-nav',
      animating: 'iis-is-animating',
      touchEnabled: 'iis-touch-enabled',
      touching: 'iis-is-touching',
      directionPrevious: 'iis-direction-previous',
      directionNext: 'iis-direction-next'
    }
});

var slider2 = new IdealImageSlider.Slider({
  selector: '#slider2',
  height: '7:2', // "auto" | px value (e.g. 400) | aspect ratio (e.g. "16:9")
  initialHeight: 400, // for "auto" and aspect ratio
  maxHeight: null, // for "auto" and aspect ratio
  interval: 4000,
  transitionDuration: 700,
  effect: 'slide',
  disableNav: false,
  keyboardNav: true,
  previousNavSelector: '',
  nextNavSelector: '',
  classes: {
    container: 'ideal-image-slider',
    slide: 'iis-slide',
    previousSlide: 'iis-previous-slide',
    currentSlide: 'iis-current-slide',
    nextSlide: 'iis-next-slide',
    previousNav: 'iis-previous-nav',
    nextNav: 'iis-next-nav',
    animating: 'iis-is-animating',
    touchEnabled: 'iis-touch-enabled',
    touching: 'iis-is-touching',
    directionPrevious: 'iis-direction-previous',
    directionNext: 'iis-direction-next'
  }
});

var slider3 = new IdealImageSlider.Slider({
  selector: '#slider3',
  height: '7:2', // "auto" | px value (e.g. 400) | aspect ratio (e.g. "16:9")
  initialHeight: 400, // for "auto" and aspect ratio
  maxHeight: null, // for "auto" and aspect ratio
  interval: 4000,
  transitionDuration: 700,
  effect: 'slide',
  disableNav: false,
  keyboardNav: true,
  previousNavSelector: '',
  nextNavSelector: '',
  classes: {
    container: 'ideal-image-slider',
    slide: 'iis-slide',
    previousSlide: 'iis-previous-slide',
    currentSlide: 'iis-current-slide',
    nextSlide: 'iis-next-slide',
    previousNav: 'iis-previous-nav',
    nextNav: 'iis-next-nav',
    animating: 'iis-is-animating',
    touchEnabled: 'iis-touch-enabled',
    touching: 'iis-is-touching',
    directionPrevious: 'iis-direction-previous',
    directionNext: 'iis-direction-next'
  }
});

var slider4 = new IdealImageSlider.Slider({
  selector: '#slider4',
  height: '7:2', // "auto" | px value (e.g. 400) | aspect ratio (e.g. "16:9")
  initialHeight: 400, // for "auto" and aspect ratio
  maxHeight: null, // for "auto" and aspect ratio
  interval: 4000,
  transitionDuration: 700,
  effect: 'slide',
  disableNav: false,
  keyboardNav: true,
  previousNavSelector: '',
  nextNavSelector: '',
  classes: {
    container: 'ideal-image-slider',
    slide: 'iis-slide',
    previousSlide: 'iis-previous-slide',
    currentSlide: 'iis-current-slide',
    nextSlide: 'iis-next-slide',
    previousNav: 'iis-previous-nav',
    nextNav: 'iis-next-nav',
    animating: 'iis-is-animating',
    touchEnabled: 'iis-touch-enabled',
    touching: 'iis-is-touching',
    directionPrevious: 'iis-direction-previous',
    directionNext: 'iis-direction-next'
  }
});

var slider5 = new IdealImageSlider.Slider({
  selector: '#slider5',
  height: '7:2', // "auto" | px value (e.g. 400) | aspect ratio (e.g. "16:9")
  initialHeight: 400, // for "auto" and aspect ratio
  maxHeight: null, // for "auto" and aspect ratio
  interval: 4000,
  transitionDuration: 700,
  effect: 'slide',
  disableNav: false,
  keyboardNav: true,
  previousNavSelector: '',
  nextNavSelector: '',
  classes: {
    container: 'ideal-image-slider',
    slide: 'iis-slide',
    previousSlide: 'iis-previous-slide',
    currentSlide: 'iis-current-slide',
    nextSlide: 'iis-next-slide',
    previousNav: 'iis-previous-nav',
    nextNav: 'iis-next-nav',
    animating: 'iis-is-animating',
    touchEnabled: 'iis-touch-enabled',
    touching: 'iis-is-touching',
    directionPrevious: 'iis-direction-previous',
    directionNext: 'iis-direction-next'
  }
});

(function() {
	let d = document;

	function init() {
		//Links 
		let anchor1Link  = d.getElementById('anchor1Link');
		let anchor2Link  = d.getElementById('anchor2Link');
		let anchor3Link  = d.getElementById('anchor3Link');
		let anchor4Link  = d.getElementById('anchor4Link');
		let anchor5Link  = d.getElementById('anchor5Link');
		
		//Anchors
		let anchor1      = d.getElementById('anchor1');
		let anchor2      = d.getElementById('anchor2');
		let anchor3      = d.getElementById('anchor3');
		let anchor4      = d.getElementById('anchor4');
		let anchor5      = d.getElementById('anchor5');
		
		anchor1Link.addEventListener('click', (e) => { scrollTo(anchor1, e) }, false);
		anchor2Link.addEventListener('click', (e) => { scrollTo(anchor2, e) }, false);
		anchor3Link.addEventListener('click', (e) => { scrollTo(anchor3, e) }, false);
		anchor4Link.addEventListener('click', (e) => { scrollTo(anchor4, e) }, false);
		anchor5Link.addEventListener('click', (e) => { scrollTo(anchor5, e) }, false);
		
  }
  
	var requestAnimFrame = (function() {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	function scrollTo(to, callback, duration = 1500) {
		to = to.offsetTop;
		
		var start = document.documentElement.scrollTop;
		var change = to - start;
		var currentTime = 0;
		var increment = 20;
		
		var animateScroll = function() {
			currentTime += increment;
			var val = Math.easeInOutQuad(currentTime, start, change, duration);
			document.scrollingElement.scrollTop = val;
			
			if (currentTime < duration) {
				requestAnimFrame(animateScroll);
			}
			else {
				if (callback && typeof(callback) === 'function') {
					callback();
				}
			}
		};
		
		animateScroll();
	}

	init();
})();

Math.easeInOutQuad = function(t, b, c, d) {
	t /= d / 2;
	if (t < 1) {
		return c / 2 * t * t + b
	}
	t--;
	return -c / 2 * (t * (t - 2) - 1) + b;
};
