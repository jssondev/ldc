Object.defineProperty(String.prototype, 'hashCode', {
    value: function() {
      var hash = 0, i, chr;
      for (i = 0; i < this.length; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0;
      }
      return hash;
    }
  });


var button = document.getElementById('access-button');
button.onclick = function(e) {
    location.replace("./luceritos_exclusive_content.html");
}

var input = document.getElementById("access-input");
input.oninput = function(e) {
    if(input.value.length == 0) {
        input.style.width = "13.2vw";    
    } else {
        input.style.width = (((input.value.length + 1) * 1) + 2.2) + "vw";
    }
    if(input.value.hashCode() == -161880892) {
        input.disabled = true;
        button.disabled = false;
    }
}




